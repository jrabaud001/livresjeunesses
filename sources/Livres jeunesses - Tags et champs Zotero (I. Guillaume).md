
## Tags

### Formats

- carré
- leporello 
- format en hauteur 
- oblong
- square
- concertina book
- portrait format

### Genres 

- abécédaire
- bestiaire
- imagier
- livre à compter
- panorama
- panoramas
- alphabet book
- picture book
- counting book
- animal picture book

### Palimpsestes

- Charles Perrault
- Jacob et Wilhelm Grimm
- conte
- tale 
- fairytale

### Techniques 

- découpe
- découpé
- pliage
- pliure
- pliures
- pop up
- volet
- collage
- collages
- gravure
- photographie
- photographies
- pochoir
- die-cut
- fold
- engraving
- stencil
- flaps
- photographs

### Matières

- calque
- page miroir
- tissu
- tissus
- spirale
- tactile
- tracing paper
- mirrored page
- fabric
- spiral-bound
- tactile book

### Caractéristiques diverses

- étui
- portfolio
- triptyque
- typographie
- métamorphose
- métamorphoses
- métamorphosent
- case
- triptych
- typography
- metamorphosis

## Champs Zotero

- Type de document
- Auteur 
  - *Est-il possible d'ajouter la parenthèse avec l'année de naissance et, selon les cas, celle de la mort ?*
- Résumé
- ~~Collection~~
- ~~N° dans la collection~~
- ~~Volume~~
- ~~Edition~~
- Lieu
- Maison d’édition
- Date
- ~~Langue~~
- ISBN
- ~~Titre abrégé~~
- ~~URL~~
- Consulté
- ~~Archive~~
- Localisation dans l’archive
- ~~Autorisation~~
- Extra