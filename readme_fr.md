﻿<Les textes d’aide sont écrits entre chevrons et sont destinés à être supprimés avant toute sauvegarde>

<Un README : Pourquoi ?

***La documentation d’un jeu de données doit être suffisante pour permettre à n’importe quel réutilisateur de comprendre et d’évaluer sa qualité. Le README fourni des informations complémentaires et accessibles lorsqu’elles ne sont pas déjà mises à disposition dans les métadonnées du jeu de données, dans les métadonnées des fichiers, et/ou dans des fichiers associés, ou des fichiers accessibles à long terme sur des services d’hébergement (entrepôt de fichiers ou publication). Dans ce dernier cas, nous vous prions d’inclure les URLs des documents en question ou leurs références***>

<Privilégier les formats text document (.txt), ou markdown (.md)>

Modèle de Fichier RDG README --- Général --- Version: 0.1 (2022-11-22) 

Ce fichier README a été généré le 2023-05-02 par Murièle Ropars, Marc Odunlami et Julien Rabaud.

Dernière mise-à-jour le : 2023-05-02.

# INFORMATIONS GENERALES

## Titre du jeu de données : Les livres d’artistes pour la jeunesse dans l’agglomération Pau Béarn Pyrénées
 
## DOI: <en cours>
 
## Adresse de contact : 

isabelle.guillaume@univ-pau.fr
 
<Ci-après suit une liste d’éléments suggérés pour vous aider à enrichir, si nécessaire, votre documentation. La pertinence de certains dépend de la discipline du jeu de données ou du contexte de production>

<***Supprimer toute section non-applicable***>

# INFORMATIONS METHODOLOGIQUES

## Conditions environnementales / experimentales : 

## Description des sources et méthodes utilisées pour collecter et générer les données :

Un fichier Word avec 223 notices rédigées par Isabelle Guillaume (UPPA - ALTER). Propriétés saisies dans un fichier csv un utilisant les vocabulaires Dublin Core, Vivo (placeOfPublication) et Bibframe (isbn) correspondants à un modèle de ressource Omeka S.

## Méthodes de traitement des données :

Notebook (python) pour générer un fichier YAML par ligne du csv pour générer des fichiers html de chaque notice avec Pandoc.



## Autres informations contextuelles :

Logiciels utilisés :

- [Omeka S](https://omeka.org/s/) : version 3.2.1
  - Module [CSV Import](https://omeka.org/s/docs/user-manual/modules/csvimport/) version 2.2.0
- Python : version 3.8.5
- [Pandoc](https://pandoc.org/) : version 3.1.1



# APERCUS DES DONNEES ET FICHIERS


## Arborescence/plan de classement des fichiers :

### Dossier `Omeka` 

Contient le fichier csv chargé dans *Omeka S* avec le plugin *CSV Import* : `GrilleSaisieDC.csv` 

#### Sous-dossier `ModeleSaisie`

Le modèle de saisie à importer dans Omeka S : `LivreJeunesse.json`


### Dossier Notices

Le *notebook* Jupyter (python) pour générer des fichiers YAML.
Une copie du fichier `GrilleSaisieDC.csv` avec colonnes renommées : `GrilleSaisieToYaml.csv`. 

#### Sous-dossier `templates`

la template *html* pour *Pandoc*


# INFORMATIONS SPECIFIQUES AUX DONNEES POUR : `GrilleSaisieDC.csv`

<Le cas échéant, reproduire cette section pour chaque dossier ou fichier.
Les éléments se répétant peuvent être expliqués dans une section initiale commune.>

<Pour les données tabulaires, fournir un dictionaire des données/manuel de codage contenant les information suivantes :>

## Liste des variables/entêtes de colonne :

- "dcterms:identifier"
- "dcterms:type"
- "dcterms:creator"
- "dcterms:title"
- "dcterms:language"
- "vivo:placeOfPublication"
- "dcterms:publisher"
- "dcterms:date"
- "dcterms:created"
- "dcterms:format"
- "dcterms:extent"
- "dcterms:description-fr"
- "dcterms:description-en"
- "bibo:isbn"
- "dcterms:provenance"
- "dcterms:abstract"
- "dcterms:relation"
- "dcterms:isPartOf"
- "dcterms:hasVersion"
- "dcterms:isVersionOf"
- "dcterms:bibliographicCitation"
- "dcterms:subject"
  - "Genre"
  - "Palimpseste"
  - "Technique"
  - "Matière"
  - "CaractéristiqueDiverse"
  - "dcterms:dateCopyrighted"
- "media"

Pour chaque nom de variable ou entête de colonne, indiquer :
 
	-- le nom complet de la variable sous forme “lisible par les humains” ; 
	-- la description de la variable ; 
	-- unité de mesure, si applicable ; 
	-- séparateur décimal *i.e.* virgule ou point, si applicable ; 
	-- valeurs autorisées : liste ou plage de valeurs, ou domaine ;
	-- format, si applicable, e.g. date>

## Code des valeurs manquantes :

<Definir les codes ou symboles utilisés pour les valeurs manquantes.>

## Informations additionnelles : 

<Toute information que vous jugez utile pour mieux comprendre le fichier>