# LivresJeunesses

> Notices et catalogue des livres d'artistes pour la jeunesse des collections du Réseau des Médiathèques de l'Agglomération paloise et du Service Commun de la Documentation de l'Université de Pau et des Pays de l'Adour.

## Source

Un fichier Word avec 223 notices rédigées par Isabelle Guillaume (UPPA - ALTER).

## Saisie

Dans une feuille de calcul (csv) :
- colonnes : propriétés Dublin Core, Vivo (placeOfPublication) et Bibframe (isbn) correspondantes à la template OmekaS
- 1 ligne par livre
- valeurs multiples séparées par une virgule (creator, language, placeOfPublication, publisher, isbn, provenance, subject)

## Export csv

### Pour Omeka S

Séparateur `;`

### Pour Yaml

Copie simple du fichier csv pour Omeka avec renommage des colonnes

fichier ipynb (jupyter) pour création d'un fichier yaml par ligne

## Templates html et markdown

Dans le dossier `NoticesTextes/templates`

## Conversions (dans le dossier `NoticesTexte/yaml`)

### création des fichiers html

```sh
$> find . -name "*.yaml" | while read i; do pandoc ../vide.md --metadata-file="$i" --template=../templates/templateLAJ.html -o ../html/"${i%.*}.html"; done
```

### création des fichiers markdown

```sh
$> find . -name "*.yaml" | while read i; do pandoc ../vide.md --metadata-file="$i" --template=../templates/templateLAJ.md -o ../md/"${i%.*}.md"; done
```

notes

requête url

item?item_set_id[0]=14290&sort_by=title&sort_order=asc