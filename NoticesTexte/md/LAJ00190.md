---
id: LAJ00190
title: Quinze à tous les coups !
keywords: Carré, livre à compter, conte, découpe, pop up
---

### [[Květa PACOVSKÁ (née en 1928)]]

#### *Quinze à tous les coups !*

---

- Paris : [[Minedition]], 2015.
- ISBN : 978-2-35413-282-8

- [[Réseau des médiathèques]]

---

Livre carré (25 x 25 cm) avec des découpes et des pop-up, sous-titré *Le
mystère de la sorcière*, dans lequel l'artiste tchèque s'inspire d'un
extrait de *Faust* de Johann Wolfgang von Goethe pour faire surgir une
sorcière jouant avec les chiffres.

> Square (9.8 x 9.8 in) book with die-cuts and pop-ups, subtitled *Le
mystère de la sorcière* \[*The Mystery of the Witch*\]. The Czech
artist's inspiration for her witch playing with numbers was an extract
from *Faust* by Johann Wolfgang von Goethe.



---