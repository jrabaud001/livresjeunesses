---
id: LAJ00160
title: Bonne nuit à tous
keywords: en hauteur, découpe, volets
---

### [[Bruno MUNARI (1907-1998)]]

#### *Bonne nuit à tous*

---

- Paris : [[Seuil jeunesse]], 2006.
- ISBN : 978-2-02-088802-5

- [[Réseau des médiathèques]]

---

Livre au format en hauteur (32 x 24 cm) avec des illustrations, des
découpes et des volets à soulever.

> Book in portrait (12.6 x 9.4 in) format with illustrations, die-cuts
and liftable flaps.

---

Il s'agit de la réédition d'un livre conçu comme l'un des volumes de la
série *I libri Munari* parue chez Mondadori en 1945 mais publié, pour la
première fois, en 1997 à Mantoue par Maurizio Corraini. En France,
*Buona notte a tutti* a d'abord été traduit et édité en 1997 sous le
titre *Bonne nuit tout le monde* par Les trois ourses pour Corraini avec
un tirage de sept cents exemplaires.



---