---
id: LAJ00084
title: La nuit du visiteur
keywords: en hauteur, conte, typographie
---

### [[Benoît JACQUES (né en 1958)]]

#### *La nuit du visiteur*

---

- Montigny-sur-Loing : [[Benoît Jacques books]], 2008.
- ISBN : 2-916683-09-7

- [[Réseau des médiathèques]]

---

Livre sous jaquette, au format en hauteur (20 x 16 cm), qui met en scène
une version mi-humoristique, mi-inquiétante, du conte *Le Petit Chaperon
rouge* en jouant sur la typographie.

> Book with a dust jacket, in portrait (7.9 x 6.3 in) format, presenting
a semi-humorous, semi-disturbing version of the tale *Little Red Riding
Hood* with a play on typography.



---