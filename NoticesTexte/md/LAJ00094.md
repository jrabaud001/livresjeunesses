---
id: LAJ00094
title: Friends in Nature. Fun for Children
keywords: Carré, pliage, étui, métamorphoses
---

### [[Katsumi KOMAGATA (né en 1953)]]

#### *Friends in Nature. Fun for Children*

---

- Tokyo : [[Kasei-Sha]], 1992.
- ISBN : 978-4-03-132080-1

- [[Réseau des médiathèques]]

---

Portfolio composé d'un étui et d'une chemise carrés (13 x 13 cm)
contenant quatre livrets de même format dotés d'un titre. Leur image
initiale se métamorphose à mesure que les pages sont tournées, évoquant
ainsi un ciel changeant (*Sun & Clouds*), l'apparition d'un chat devant
des souris (*Cats & Rats*), les amitiés nouées par un éléphant et la
plantation d'un bois (*Friends*, *Woods*).

> Portfolio comprising a case and a folder in square (5.1 x 5.1 in)
format containing four booklets in the same format, each bearing a
title. In each booklet, the opening image metamorphoses as the pages are
turned, thereby depicting a changing sky (*Sun & Clouds*), a cat
appearing in front of rats (*Cats & Rats*), the friendships kindled by
an elephant (*Friends*) and the planting of woodland (*Woods*).

---

*Friends in Nature* est le huitième des dix volumes de la série *Little
Eyes*.


---

Sur l'œuvre de Katsumi Komagata, on peut consulter : Jean Widmer Jean,
*Les livres de Katsumi Komagata*, Paris, Les trois ourses, 2013.


---