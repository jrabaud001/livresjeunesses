---
id: LAJ00055
title: Macao et Cosmage ou l'expérience du bonheur
keywords: Carré
---

### [[ÉDY-LEGRAND (1892-1970)]]

#### *Macao et Cosmage ou l'expérience du bonheur*

---

- Paris : [[Circonflexe]], 2000.
- ISBN : 2-87833-271-7

- [[Réseau des médiathèques]], [[Bibliothèque universitaire]]

---

Le peintre Édy-Legrand donne à une fable écologique la forme d'un livre
carré (33 x 33 cm) aux illustrations et aux motifs décoratifs influencés
par le japonisme.

> The painter Édy-Legrand presents this ecological fable in a square
(13.0 x 13.0 in) book with illustrations and decorative motifs
influenced by Japonism.

---

Il s'agit de la première réédition de ce livre publié en 1919 par La
nouvelle revue française avec un tirage de cinq cents exemplaires.



---