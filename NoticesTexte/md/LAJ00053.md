---
id: LAJ00053
title: Des petits clous de rien du tout
keywords: Oblong
---

### [[Sophie CURTIL (née en 1949)]]

#### *Des petits clous de rien du tout*

---

- Paris : [[Les trois ourses]], 2007.
- ISBN : 978-2-9518639-3-4

- [[Réseau des médiathèques]]

---

Livre au format oblong (15 x 21 cm) qui joue avec les formes à partir de
couleurs pulvérisées sur de simples clous.

> Book in oblong (5.9 x 8.3 in) format that plays with shapes
spray-painted in different colours on nails.

---

La maquette du livre a été réalisée par Katsumi Komagata.



---