---
id: LAJ00207
title: De l'autre côté des étoiles
keywords: en hauteur, pop up
---

### [[Philippe UG (né en 1958)]]

#### *De l'autre côté des étoiles*

---

- Paris : [[Les grandes personnes]], 2020.
- ISBN : 978-2-36193-605-1

- [[Réseau des médiathèques]]

---

Dans ce livre au format en hauteur (23 x 14 cm), Philippe UG transforme
en sept pop up le voyage d'extraterrestres devenus explorateurs.

> In this book in portrait (9.1 x 5.5 in) format, Philippe UG transforms
the voyage of extraterrestrials-turned-explorers into seven pop-ups.



---