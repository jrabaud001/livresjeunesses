---
id: LAJ00079
title: Traces of Ancestors Lost. Traces d'ancêtres perdus
keywords: Oblong, photographies
---

### [[Tana HOBAN (1917-2006)]]

#### *Traces of Ancestors Lost. Traces d'ancêtres perdus*

---

- Paris : [[Les trois ourses]], 2001.
- ISBN : 2-9513083-3-7

- [[Réseau des médiathèques]]

---

Livre oblong (21 x 29 cm) composé de photographies en noir et blanc
représentant des traces gravées dans divers sols.

> Oblong (8.3 x 11.4 in) book composed of black-and-white photographs
showing traces engraved in different ground surfaces.



---