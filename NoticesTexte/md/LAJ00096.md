---
id: LAJ00096
title: Go around. Fun for Children
keywords: Carré, pliage, étui, métamorphoses
---

### [[Katsumi KOMAGATA (né en 1953)]]

#### *Go around. Fun for Children*

---

- Tokyo : [[Kasei-Sha]], 1992.
- ISBN : 4-03-132100-7

- [[Réseau des médiathèques]]

---

Portfolio composé d'un étui et d'une chemise carrés (13 x 13 cm)
contenant quatre étuis plus petits qui permettent de constituer quatre
images. Chaque étui contient une page cartonnée de trente-six
centimètres de long construite autour de deux images qui, grâce à un
pliage, se métamorphosent selon l'angle de vue.

> Portfolio comprising a case and a folder in square (5.1 x 5.1 in)
format containing four smaller cases that allow four images to be put
together. Each case contains a paperboard page, thirty-six centimetres
long (14.2 in), constructed around two images which, being folded,
metamorphose depending on the viewing angle.

---

*Go around* est le dixième et dernier volume de la série *Little Eyes*.


---

Sur l'œuvre de Katsumi Komagata, on peut consulter : Jean Widmer Jean,
*Les livres de Katsumi Komagata*, Paris, Les trois ourses, 2013.


---