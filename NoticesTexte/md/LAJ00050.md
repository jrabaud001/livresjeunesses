---
id: LAJ00050
title: La roue
keywords: Carré
---

### [[Louise-Marie CUMONT (née en 1957)]]

#### *La roue*

---

- Nantes : [[MeMo]], 2013.
- ISBN : 978-2-35289-165-9

- [[Réseau des médiathèques]]

---

Livre carré (15 x 15 cm) qui explore l'espace de la page à partir d'un
personnage qui s'entraîne à faire la roue.

> Square (5.9 x 5.9 in) book that explores the space of a page through a
character practising cartwheels.

---

Il s'agit de la version imprimée sur papier d'un livre en tissu, au
titre identique, réalisé en 1991 à la main en soixante-quinze
exemplaires signés et numérotés.



---