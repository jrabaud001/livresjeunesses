---
id: LAJ00028
title: Vieilles chansons et rondes
keywords: Oblong
---

### [[Maurice BOUTET DE MONVEL (1851-1913)]]

#### *Vieilles chansons et rondes*

---

- Paris : [[L'école des loisirs]], 1981.
- ISBN : 2-211-07447-2

- [[Réseau des médiathèques]]

---

Recueil oblong (23 x 27 cm) de textes et de partitions dans lequel
Maurice Boutet de Monvel rompt avec l'illusion de la perspective pour
donner une fonction décorative aux images et pour exploiter leurs
ressources rythmiques en écho au contenu musical du livre.

> Oblong (9.1 x 10.6 in) album of words and music in which Maurice
Boutet de Monvel breaks with the illusion of perspective to give the
images a decorative function and to exploit their rhythmic resources,
echoing the book's musical content.

---

Il s'agit d'une réédition du livre publié à Paris par Plon, Nourrit et
Cie en 1883. Le titre intérieur indique *Vieilles chansons et rondes
pour les petits enfants*.



---