---
id: LAJ00049
title: En voiture
keywords: Oblong, tissu
---

### [[Louise-Marie CUMONT (née en 1957)]]

#### *En voiture*

---

- Nantes : [[MeMo]], 2011.
- ISBN : 978-2-35289-122-2

- [[Réseau des médiathèques]]

---

Livre oblong (22 x 26 cm) à la couverture cartonnée qui raconte un
voyage en voiture grâce à l'assemblage de pièces de tissu.

> Hardcover oblong (8.7 x 10.2 in) book that recounts a car trip, using
an assembly of pieces of fabric.



---