---
id: LAJ00139
title: Aquarium
keywords: Oblong
---

### [[Fanette MELLIER (née en 1977)]]

#### *Aquarium*

---

- Strasbourg : [[Éditions du livre]], 2018.
- ISBN : 979-10-90475-27-9

- [[Réseau des médiathèques]]

---

Livre oblong (19 x 22 cm) à la couverture muette dont la richesse des
compositions graphiques se révèle pleinement quand les pages sont
placées face à la lumière.

> Oblong (7.5 x 8.7 in) book with a textless cover, whose wealth in
graphic compositions is fully revealed when the pages are held up to
light.


---

Sur le travail de Fanette Mellier, on peut se reporter à : Sandrine
Maillet, « Fanette Mellier : travailler la matière textuelle », dans
*Revue de la BNF* n° 43, 2013/1, p. 46-49.


---