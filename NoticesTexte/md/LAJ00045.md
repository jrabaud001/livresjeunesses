---
id: LAJ00045
title: Larmes
keywords: Carré, tissu
---

### [[Louise-Marie CUMONT (née en 1957)]]

#### *Larmes*

---

- Nantes : [[MeMo]], 2007.
- ISBN : 978-2-910391-97-3

- [[Réseau des médiathèques]]

---

Livre carré (28 x 28 cm) qui représente des regards, des visages et des
corps avec un tissu de camouflage évocateur de guerre.

> Square (11.0 x 11.0 in) book representing expressions, faces and
bodies, using a camouflage fabric suggestive of war.

---

Il s'agit de la transposition sur papier d'un livre en tissu, au titre
identique, réalisé à la main en 2004 à soixante-quinze exemplaires
numérotés et signés. Pour l'éditrice Christine Morault, la
transformation du livre en tissu au tirage limité en édition courante
est exemplaire de la volonté de MeMo d'offrir à tous une éducation
artistique du regard : « C'est un livre pour les tout-petits et un livre
d'artiste pour les adultes. Sur un tissu de camouflage, des yeux brodés
font apparaître des visages qui s'affrontent, des corps, des silhouettes
dans l'ombre. Ce livre parle de guerre, mais sans mots, dans la nudité
d'une révélation. Le lecteur va à la recherche de l'œuvre, et cet effort
est lui-même créateur. Le conducteur offset qui l'a imprimé en a livré
le plus juste sentiment en le comparant à la libre composition d'images
auquel se livre celui qui contemple des nuages... Beaucoup pensaient
impossible le passage du livre original, en tissu, au support papier.
Pour parvenir à rendre suffisamment présent l'effet du tissu, nous avons
séparé puis imprimé à part la trame du tissu et ses couleurs. Après un
premier essai désastreux, nous avons enfin le sentiment d'être au plus
près, autant qu'il est possible, du livre en tissu » (Christine Morault,
« Les éditions MeMo. Entre papier et encres : une histoire de
transmission », dans *Revue de l'Association des bibliothèques de
France* n° 32, mai 2007, p. 75)


---

Christine Morault, « Les éditions MeMo. Entre papier et encres : une
histoire de transmission », dans *Revue de l'Association des
bibliothèques de France* n° 32, mai 2007, p. 75


---