---
id: LAJ00073
title: Raies, points, pois
keywords: Oblong, photographies
---

### [[Tana HOBAN (1917-2006)]]

#### *Raies, points, pois*

---

- Paris : [[Kaléidoscope]], 2007.
- ISBN : 978-2-877-67512-3

- [[Réseau des médiathèques]]

---

Livre oblong (21 x 26 cm) composé d'une suite de photographies en
couleur contenant des points ou des rayures.

> Oblong (8.3 x 10.2 in) book composed of a series of colour photographs
featuring dots or stripes.

---

Il s'agit de la première édition française de *Dots, Spots, Speckles and
Stripes* publié à New York par Greenwillow Books en 1987.



---