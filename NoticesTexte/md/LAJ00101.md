---
id: LAJ00101
title: \[Quand le ciel est bleu, la mer est bleue aussi\]
keywords: Carré, découpe
---

### [[Katsumi KOMAGATA (né en 1953)]]

#### *\[Quand le ciel est bleu, la mer est bleue aussi\]*

---

- Tokyo : [[One stroke]], 1995.
- Sans ISBN.

- [[Réseau des médiathèques]]

---

Le livre se présente dans un format carré (11 x 11 cm). Il se déplie en
une feuille cartonnée de 44 x 44 cm dont les découpes et les images
accompagnent quinze questions et leurs réponses scientifiques, en
alphabet syllabaire japonais, portant sur les couleurs. La première
reprend le titre du livre : « Quand le ciel est bleu, la mer est bleue
elle aussi. Pourquoi ? »

> The book, presented in square (4.3 x 4.3 in) format, unfolds into a
paperboard sheet of 17.3 x 17.3 in, whose die-cuts and images accompany
fifteen questions on colours, and their scientific answers, in Japanese
syllabic script. The first question reproduces the book's title, "Quand
le ciel est bleu, la mer est bleue elle aussi. Pourquoi ? \[Sea is blue
when sky is blue. Why?\]".


---

Sur l'œuvre de Katsumi Komagata, on peut consulter : Jean Widmer Jean,
*Les livres de Katsumi Komagata*, Paris, Les trois ourses, 2013.


---