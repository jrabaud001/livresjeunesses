---
id: LAJ00092
title: What color ? Learning for Children
keywords: Carré, pliage, découpe, étui, tryptique, métamorphoses
---

### [[Katsumi KOMAGATA (né en 1953)]]

#### *What color ? Learning for Children*

---

- Tokyo : [[Kasei-Sha]], 1991.
- ISBN : 978-4-03-132060-3

- [[Réseau des médiathèques]]

---

Portfolio composé d'un étui et d'une chemise carrés (13 x 13 cm)
contenant douze triptyques de même format, cartonnés, pliés, avec des
découpes ainsi que des cercles de douze couleurs différentes qui se
métamorphosent en plantes et en animaux quand les triptyques sont
dépliés.

> Portfolio comprising a case and a folder in square (5.1 x 5.1 in)
format containing twelve triptychs in the same format, in paperboard,
numbered, folded, with die-cuts as well as circles in twelve different
colours, which metamorphose into plants and animals when the triptychs
are unfolded.

---

*What color ?* est le sixième des dix volumes de la série *Little Eyes.*


---

Sur l'œuvre de Katsumi Komagata, on peut consulter : Jean Widmer Jean,
*Les livres de Katsumi Komagata*, Paris, Les trois ourses, 2013.


---