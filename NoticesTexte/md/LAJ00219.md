---
id: LAJ00219
title: La crème d'âne
keywords: Leporello
---

### [[Catherine Wilkin (née en 1976)]]

#### *La crème d'âne*

---

- Goegnies-Chaussée : [[Miaw éditions]], 2008.
- Sans ISBN.

- [[Réseau des médiathèques]]

---

Catherine Wilkin transpose une situation vécue avec sa fille en mettant
en scène des animaux dans ce leporello (15 x 360 cm) présenté dans une
chemise avec des rabats.

> Catherine Wilkin transposes a situation she once experienced with her
daughter in this concertina book (5.9 x 141.7 in) with animal
characters, presented in a folder with flaps.

---

L'exemplaire du réseau des médiathèques est numéroté 15/100.



---