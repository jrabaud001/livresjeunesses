---
id: LAJ00166
title: Le petit roi des fleurs
keywords: en hauteur, conte, collage, découpe, volets
---

### [[Květa PACOVSKÁ (née en 1928)]]

#### *Le petit roi des fleurs*

---

- Paris : [[Minedition]], 2009.
- ISBN : 978-2-35413-053-4

- [[Réseau des médiathèques]]

---

Livre au format en hauteur (19 x 15 cm), sous étui, avec une découpe
carrée centrale traversant la couverture et plusieurs pages dans lequel
l'artiste tchèque fait entrer un conte de Hans Gärtner dans son univers
visuel caractérisé par les collages et les couleurs vives.

> Book in portrait (7.5 x 5.9 in) format presented in a case, with a
central square die-cut punched into the cover and several pages, in
which the Czech artist brings a tale by Hans Gärtner into her visual
universe characterised by collages and vivid colours.

---

Il s'agit de la troisième édition française, après celles de L'école des
loisirs en 1992 et de Nord-Sud en 2006, de *Der kleine Blumenkönig*
publié à Salzbourg par Michael Neugebauer Press en 1991.



---