---
id: LAJ00124
title: Blanche Neige
keywords: Leporello, Jacob & Wilhelm Grimm, conte
---

### [[Warja LAVATER (1913-2007)]]

#### *Blanche Neige*

---

- Paris : [[Adrien Maeght]], 1974.
- Sans ISBN.

- [[Bibliothèque universitaire]]

---

Leporello (16 x 438 cm), sous deux plats de toile verte, avec des
dessins lithographiés dans lesquels Warja Lavater réinterprète le conte
de Jacob et Wilhelm Grimm grâce à des symboles qui remplacent le décor
ainsi que les personnages et les objets dotés d'une fonction actantielle
selon un code indiqué dans une légende en trois langues (français,
anglais, allemand). Le titre intérieur indique : *Blanche Neige une
imagerie d'après le conte*.

> Concertina book (6.3 x 172.4 in) with green canvas front and back
covers, and lithographed drawings in which Warja Lavater reinterprets
Jacob and Wilhelm Grimm's fairytale "Snow White", using symbols to
replace the settings as well as the characters and objects given an
actantial function according to a code indicated in a legend in three
languages (French, English, German). The title page reads: *Blanche
Neige une imagerie d'après le conte* \[*Snow White Imagery After the
Tale by Perrault*\].


---

Sur les livres de Warja Lavater, on peut se reporter à : Christophe
Meunier, « Les imageries de Warja Lavater : une mise en espace des
contes... », billet mis en ligne sur le blog « Les territoires de
l'album. Espaces et spatialités dans les albums pour enfants » le 18
janvier 2013 et dont la dernière consultation date du 30 novembre 2021,
<https://lta.hypotheses.org/396>.


---