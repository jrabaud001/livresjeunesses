---
id: LAJ00141
title: BASIC SPACE
keywords: Leporello, découpe, pliage, étui
---

### [[Fanny MILLARD (née en 1980)]]

#### *BASIC SPACE*

---

- Bordeaux : [[Association Extra]], 2015.
- ISBN : 978-2-9552904-0-8

- [[Réseau des médiathèques]], [[Bibliothèque universitaire]]

---

*BASIC SPACE*, qui est sous-titré *Le premier livre maquette*, se
présente dans un étui (12 x 12 cm) en papier épais bleu avec des
découpes et il est accompagné d'un livret en forme de leporello (12 x 72
cm). Il est composé de deux leporellos perpendiculaires (12 x 60 cm) en
carton épais avec, d'un côté, des motifs orange sur fond blanc et, de
l'autre côté, des motifs blanc sur fond orange. Fanny Millard a conçu ce
livre comme une initiation à la création d'espaces. Le livret montre
quelques unes de ces créations ; il fait de celles-ci un territoire pour
les contes et il explique : « *BASIC SPACE* est une initiation ludique à
l'espace de l'architecture. Le lecteur, en déployant le livre, forme une
histoire et la fait évoluer selon les espaces qu'il crée. Son
imagination se trouve projetée dans un livre en trois dimensions ; et le
livre devient support scénographique de sa propre histoire !
Transformant l'objet livre en maquette, le lecteur devient l'architecte
de son propre projet ».

> *BASIC SPACE*, subtitled *Le premier livre maquette* \[*The First
Model Book*\], comes in a case (4.7 x 4.7 in) in die-cut thick blue
paper, and is accompanied by a booklet in the form of a leporello (4.7 x
28.3 in). The model book is composed of twoperpendicular leporellos (4.7
x 23.6 in) in thick cardboard, with orange motifs against a white
background on one side, and white motifs against an orange background on
the other side. Fanny Millard designed this book as an introduction to
space creation. The booklet shows a few examples of creations, treating
them as a domain for tales, and explains: "\[*BASIC SPACE* is a playful
initiation to architectural space. As the reader deploys the book, s/he
will make up a story and develop it according to the spaces s/he
creates. The reader's imagination is projected into a three-dimensional
book --- and the book becomes a scenographic prop of his/her own story!
Transforming the book-object into a model, the reader becomes the
architect of his/her own project\]".



---