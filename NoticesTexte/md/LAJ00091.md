---
id: LAJ00091
title: 1 to 10. Learning for Children
keywords: Carré, pliage, découpe, étui, tryptique, métamorphoses
---

### [[Katsumi KOMAGATA (né en 1953)]]

#### *1 to 10. Learning for Children*

---

- Tokyo : [[Kasei-Sha]], 1991.
- ISBN : 4-03-132050-7

- [[Réseau des médiathèques]]

---

Portfolio composé d'un étui et d'une chemise carrés (13 x 13 cm)
contenant douze triptyques de même format, cartonnés, pliés, avec des
découpes ainsi que des cercles noirs, oranges et verts sur fond blanc.
Quand les triptyques sont dépliés, les cercles s'ajoutent ou se
multiplient.

> Portfolio comprising a case and a folder in square (5.1 x 5.1 in)
format containing twelve triptychs in the same format, in paperboard,
numbered, folded, with die-cuts and black, orange and green circles
against white backgrounds. When the triptychs are unfolded, the circles
are added together or multiplied.

---

*1 to 10* est le cinquième des dix volumes de la série *Little Eyes*.


---

Sur l'œuvre de Katsumi Komagata, on peut consulter : Jean Widmer Jean,
*Les livres de Katsumi Komagata*, Paris, Les trois ourses, 2013.


---