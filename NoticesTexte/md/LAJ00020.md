---
id: LAJ00020
title: J'additionne
keywords: Carré, livre à compter
---

### [[Anne BERTIER (née en 1956)]]

#### *J'additionne*

---

- Nantes : [[MeMo]], 2012.
- ISBN : 978-2-35289-181-9

- [[Réseau des médiathèques]]

---

Dans ce livre carré (15,5 x 15,5 cm), Anne Bertier additionne les
chiffres en combinant les triangles dans des constructions géométriques.

> In this square (6.1 x 6.1 in) book, Anne Bertier represents additions
by combining triangles in geometric constructions.



---