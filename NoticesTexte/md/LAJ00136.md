---
id: LAJ00136
title: Dans la lune
keywords: Oblong
---

### [[Fanette MELLIER (née en 1977)]]

#### *Dans la lune*

---

- Strasbourg : [[Éditions du livre]], 2013.
- ISBN : 979-10-90475-10-6

- [[Réseau des médiathèques]]

---

Ce livre au format oblong (20,5 x 21,5 cm) présente, en belle page, les
variations de couleur et de lumière d'un cycle lunaire.

> This book in oblong (8.1 x 8.5 in) format presents, on its right-hand
pages, variations in colour and light over a lunar cycle.


---

Sur le travail de Fanette Mellier, on peut se reporter à : Sandrine
Maillet, « Fanette Mellier : travailler la matière textuelle », dans
*Revue de la BNF* n° 43, 2013/1, p. 46-49.


---