---
id: LAJ00043
title: The Baby's Own Aesop
keywords: Carré, gravure
---

### [[Walter CRANE (1845-1915)]]

#### *The Baby's Own Aesop*

---

- Londres : [[Routlege]], 1887.
- Sans ISBN.

- [[Bibliothèque universitaire]]

---

Livre au format carré (19 x 19 cm) dans lequel Walter Crane a composé
des gravures autour de courts textes en vers écrits par le graveur
William James Linton à partir des fables attribuées au poète grec Ésope.

> Book in square (7.5 x 7.5 in) format presenting engravings composed by
Walter Crane around short texts in verse written by the engraver William
James Linton, based on fables attributed to the Greek poet Aesop.

---

Succédant à *The Baby's Opera* en 1877 et *The Baby's Bouquet* en 1878,
*The Baby's Own Aesop* est le troisième et dernier des livres carrés
conçus par Walter Crane.



---