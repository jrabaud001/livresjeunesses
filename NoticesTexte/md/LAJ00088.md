---
id: LAJ00088
title: Meet Colors. Second Step for Babies
keywords: Carré, pliage, découpe, étui, tryptique, métamorphoses
---

### [[Katsumi KOMAGATA (né en 1953)]]

#### *Meet Colors. Second Step for Babies*

---

- Tokyo : [[Kasei-Sha]], 1990.
- ISBN : 4-03-132020-5

- [[Réseau des médiathèques]]

---

Portfolio composé d'un étui et d'une chemise au format carré (13 x 13
cm) contenant douze triptyques de même format, cartonnés, numérotés,
pliés, avec des découpes et des figures géométriques rouges, jaunes,
bleues et vertes sur des fonds blancs ou colorés. Ces figures se
métamorphosent quand les triptyques sont dépliés.

> Portfolio comprising a case and a folder in square (5.1 x 5.1 in)
format containing twelve triptychs in the same format, in paperboard,
numbered, folded, with die-cuts and red, yellow, blue and green
geometrical figures against white or coloured backgrounds. These figures
metamorphose as the triptychs are unfolded.

---

*Meet colors* est le deuxième des dix volumes de la série *Little Eyes*.


---

Sur l'œuvre de Katsumi Komagata, on peut consulter : Jean Widmer Jean,
*Les livres de Katsumi Komagata*, Paris, Les trois ourses, 2013.


---