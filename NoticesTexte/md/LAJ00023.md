---
id: LAJ00023
title: Je divise
keywords: Carré, livre à compter
---

### [[Anne BERTIER (née en 1956)]]

#### *Je divise*

---

- Nantes : [[MeMo]], 2012.
- ISBN : 978-2-35289-184-0

- [[Réseau des médiathèques]]

---

Livre carré (15,5 x 15,5 cm) dans lequel Anne Bertier représente
l'opération arithmétique en découpant des carrés et des rectangles
blancs sur le fond rouge des pages.

> Square (6.1 x 6.1 in) book in which Anne Bertier represents the
mathematical operation of division by dividing up squares and rectangles
in white on red-coloured pages.



---