---
id: LAJ00107
title: Feuilles
keywords: Oblong, découpe, tactile, métamorphoses
---

### [[Katsumi KOMAGATA (né en 1953)]]

#### *Feuilles*

---

- Talant, Paris, Tokyo : [[Les doigts qui rêvent]], [[Les trois
ourses]], [[Le centre Pompidou]], [[One stroke]], 2004.
- ISBN : 2-911782-56-9, 2-9513083-8-8, 2-84426-248-1

- [[Réseau des médiathèques]]

---

Livre tactile au format oblong (24 x 25,5 cm) dont les pages colorées et
les papiers découpés accompagnent la métamorphose de feuilles d'arbres
qui s'épanouissent puis disparaissent.

> Tactile book in oblong (9.4 x 10.0 in) format, whose coloured pages
and die-cuts accompany the metamorphosis of tree leaves that thrive,
then disappear.


---

Sur l'œuvre de Katsumi Komagata, on peut consulter : Jean Widmer Jean,
*Les livres de Katsumi Komagata*, Paris, Les trois ourses, 2013.


---