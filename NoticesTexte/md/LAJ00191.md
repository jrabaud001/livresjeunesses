---
id: LAJ00191
title: Mon chat
keywords: en hauteur
---

### [[Nathalie PARAIN (1897-1958)]]

#### *Mon chat*

---

- Nantes : [[MeMo]], 2006.
- ISBN : 978-2-910391-90-4

- [[Réseau des médiathèques]]

---

Cet album relié au format en hauteur (30,8 x 24 cm) comprend neuf pages
imprimées au verso qui mettent en scène une fillette, ses jouets et ses
animaux dans des compositions influencées par le constructivisme russe.
Le texte est signé par André Beucler.

> This hardback album in portrait (12.1 x 9.4 in) format comprises nine
pages printed on the verso, showing a girl, her toys and her animals, in
compositions influenced by Russian constructivism. The text is by André
Beucler.

---

Il s'agit de la première réédition du livre. L'édition originale date de
1930 : *Mon chat* a paru cette année-là chez Gallimard sous la double
forme d'un album et d'un portfolio contenant des pages séparées pouvant
être accrochées au mur. La maquette de la couverture du livre et cinq
planches originales au format 37 x 29,5 cm (gouache, crayons de
couleurs, aquarelle, collages) sont conservées à la Bibliothèque
nationale de France.



---