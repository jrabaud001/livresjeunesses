---
id: LAJ00061
title: Ceci ou cela ?
keywords: Oblong, calque, métamorphoses
---

### [[Dobroslav FOLL (1922-1981)]]

#### *Ceci ou cela ?*

---

- Paris : [[Les trois ourses]], 2010.
- ISBN : 978-2-9518639-6-5

- [[Réseau des médiathèques]]

---

Une grille en papier cristal zébré de gris placée dans l'un des deux
rabats de la couverture permet d'opérer des métamorphoses quand on la
glisse sur les images de de ce livre oblong (15,5 x 23,5 cm). Page après
page, « ceci » se transforme en « cela ».

> A grille in grey-striped glassine paper, placed under one of the
cover's two flaps, brings metamorphoses into view when it is slid over
the images of this oblong (6.1 x 9.3 in) book. Page after page, "*ceci*
\[this\]" transforms into "*cela* \[that\]".

---

Il s'agit de la première édition française de *Co se cemu podobá ?*
publié à Prague en 1964 chez SNDK par Dobroslav Foll, peintre, graphiste
et sculpteur.



---