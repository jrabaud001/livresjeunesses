---
id: LAJ00147
title: L' Histoire des trois oiseaux
keywords: en hauteur, découpe, volets
---

### [[Bruno MUNARI (1907-1998)]]

#### *L' Histoire des trois oiseaux*

---

- Paris : [[Seuil jeunesse]], 2002.
- ISBN : 2-02-051361-7

- [[Réseau des médiathèques]]

---

Livre broché au format en hauteur (32 x 24 cm) avec des découpes et des
volets à soulever qui réservent une série de surprises.

> Paperback book in portrait (12.6 x 9.4 in) format with die-cuts and
liftable flaps that reveal a series of surprises.

---

Il s'agit de la première édition française de *Storie di tre uccellini*,
le cinquième des sept volumes de la série *I libri Munari* publiée par
Mondadori, à Vérone en 1945.



---