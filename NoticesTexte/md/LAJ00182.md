---
id: LAJ00182
title: Cendrillon
keywords: en hauteur, Charles Perrault, conte, collage
---

### [[Květa PACOVSKÁ (née en 1928)]]

#### *Cendrillon*

---

- Paris : [[Minedition]], 2009.
- ISBN : 978-2-35413-081-7

- [[Réseau des médiathèques]]

---

Livre au format en hauteur (29 x 24 cm) dans lequel l'artiste fait
entrer le texte de Charles Perrault ainsi que la pantoufle emblématique
du conte dans un univers spectaculaire avec ses couleurs vives
structurées par le rouge et le noir, ses collages et ses jeux de miroir.

> Book in portrait (11.4 x 9.4 in) format, in which the artist brings
Charles Perrault's *Cinderella*, along with the fairytale's emblematic
slipper, into a spectacular universe with vivid colours structured by
red and black, collages and mirror effects.



---