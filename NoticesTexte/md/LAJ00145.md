---
id: LAJ00145
title: Toc toc. Qui est là ? Ouvre la porte
keywords: en hauteur, volets
---

### [[Bruno MUNARI (1907-1998)]]

#### *Toc toc. Qui est là ? Ouvre la porte*

---

- Paris : [[Seuil jeunesse]], 2004.
- ISBN : 2-02-064002-3

- [[Réseau des médiathèques]]

---

Livre broché au format en hauteur (32 x 24 cm) dont les volets à
soulever réservent une série de surprises.

> Paperback book in portrait (12.6 x 9.4 in) format with liftable flaps
that reveal a series of surprises.

---

Il s'agit de la première édition française de *Toc toc. Chi è ? Apri la
porta*, le troisième des sept volumes de la série *I libri Munari*
publiée par Mondadori, à Vérone, en 1945.



---