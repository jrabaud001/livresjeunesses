---
id: LAJ00120
title: Le petit chaperon rouge
keywords: en hauteur, Charles Perrault, conte, pop up, volets
---

### [[Dominique LAGRAULA (née en 1960)]]

#### *Le petit chaperon rouge*

---

- Paris : [[Les grandes personnes]], 2019.
- ISBN : 978-2-36193-553-5

- [[Réseau des médiathèques]]

---

Dominique Lagraula interprète le conte *Le petit chaperon rouge* dans ce
livre au format en hauteur (14 x 10,5 cm) avec des pop up et des rabats
à soulever pour lire le texte de Charles Perrault.

> Dominique Lagraula interprets *Little Red Riding Hood* in this book in
portrait (5.5 x 4.1 in) format, with pop-ups and flaps that can be
lifted to reveal Charles Perrault's text.

---

Cette édition courante s'inspire du livre *Le petit chaperon rouge*
réalisé en 2000 par l'artiste en vingt exemplaires numérotés sous une
couverture rouge en papier cartonné ondulé présentant une fenêtre dans
laquelle apparaît le titre.



---