---
id: LAJ00159
title: Libro illeggibile MN 1
keywords: Carré
---

### [[Bruno MUNARI (1907-1998)]]

#### *Libro illeggibile MN 1*

---

- Paris, Mantoue : [[Les trois ourses]], [[Corraini]], 2000.
- ISBN : 978-88-86250-15-3

- [[Réseau des médiathèques]]

---

Livret sans texte ni image au format carré (10 x 10 cm) composé de pages
de couleurs diverses, la plupart avec des découpes, sous une couverture
en carton gris, reliées par un fil noué.

> Booklet in square (3.9 x 3.9 in) format without text or images,
composed of pages in different colours, most featuring die-cuts, with a
grey cardboard cover, bound by a knotted string.

---

Il s'agit de l'édition française de *Libro illegibile MN 1* publié pour
la première fois en 1984, à Mantoue, par Maurizio Corraini. L'artiste
Miloš Cvach a décrit ainsi la découverte de ce livre dont le seul
langage est celui des textures, des couleurs et des découpes : « Le
livre gris-beige est petit, carré, dix centimètres sur dix centimètres.
Il n'est pas tout à fait carré, trois de ses angles sont coupés à
quarante-cinq degrés. J'aperçois à gauche une ficelle qui court le long
du dos du livre. Elle s'enfonce dans les découpes aux deux angles, celui
du haut et celui du bas à gauche. La troisième découpe à droite, plus
grande, laisse deviner les pages de couleur du livre » (Miloš Cvach, «
Le livre comme une œuvre : anatomie d'un livre artistique », dans *Quand
les artistes créent pour les enfants. Des objets livres pour imagine*,
Autrement, 2008, p. 40). Il a proposé une lecture de cette œuvre qui
peut se « regarder page par page » ou « comme une sculpture colorée »
(*ibid.*, p. 41) avant de conclure : « Ainsi ce minuscule livre de
presque rien permet d'imaginer tout un univers » (*ibidem.*).
`<br/>`{=html} *Libro illegibile MN 1* fait partie d'une série de «
livres illisibles » de formats divers conçus par Bruno Munari des années
1950 jusqu'à sa mort. Certains d'entre eux ont été imprimés avec des
tirages d'importance variée, comme c'est le cas de *Libro illeggibile N.
XXV* composé de papiers blancs et noirs et imprimé à Milan en 1959 en
trois cents exemplaires, de *Libro illeggibile 1996* constitué de
feuilles transparentes sous une couverture en papier bleu édité à Rome
par la galerie dell'Obélisco en 1966, de *Libro Illeggibile N.Y.1* aux
pages trouées et traversées par un fil rouge édité à New York par le
MoMa en 1967, de *Libro illegibile MN 3* réalisé avec des cartons troués
et découpés et publié à Mantoue par Maurizio Corraini en 1992, de *Libro
illegibile MN 4* fait de cartons noirs et blanc découpés et publié à
Mantoue par Maurizio Corraini en 1994 (Maffei Giorgio, *Les livres de
Bruno Munari*, Paris, Les trois ourses, 2009, p. 94, 116, 119, 192,
200). D'autres n'ont été réalisés qu'à un unique exemplaire, comme
*Primo libro illeggibile 1949* présenté à la librairie Salto de Milan en
1949, *Libro illeggibile N. 8* et *Libro illeggibile N. 15* qui datent
de 1951 (*ibid.*, p. 239 à 241). `<br/>`{=html} En 1981, dans son essai
*Da cosa nasce cosa*, Bruno Munari présente ses « livres illisibles »
comme le résultat d'une expérimentation visant à savoir à savoir « s'il
est possible de faire des matériaux du livre (exception faite du texte)
un langage visuel » (Bruno Munari, « Un livre illisible », dans *De
choses et d'autres*, Paris, Pyramid, 2016, p. 199) et il fait des
Prélivres leur prolongement.


---

Sur *Libro illegibile MN 1*, on peut lire : Miloš Cvach, « Le livre
comme une œuvre : anatomie d'un livre artistique », dans *Quand les
artistes créent pour les enfants. Des objets livres pour imagine*,
Paris, Autrement, « Le Mook Autrement », 2008, p. 40-43. `<br/>`{=html}
Sur les « livres illisibles », on peut lire : Maffei Giorgio, *Les
livres de Bruno Munari*, Paris, Les trois ourses, 2009 ; Bruno Munari, «
Un livre illisible », dans *De choses et d'autres*, Paris, Pyramid,
2016, p. 199-208 (*De choses et d'autres* est la traduction de Da cosa
nasce cosa, Bari, Laterza, 1981).


---