---
id: LAJ00224
title: La clé sous la porte
keywords: Leporello, conte, gravure
---

### [[Julia CHAUSSON (née en 1977)]]

#### *La clé sous la porte*

---

- Paris : [[imprimé par l'artiste dans son atelier]], 2014.
- Sans ISBN.

- [[Bibliothèque universitaire]]

---

Leporello (25 x 344 cm) composé de quinze gravures sur bois dans lequel
Julia Chausson interprète sept contes sous la forme d'une annonce
immobilière et d'une clé emblématique, sur la page de gauche, et d'un
décor comprenant une maison, en belle page.

> Concertina book (9.8 x 135.4 in) composed of fifteen wood engravings
in which Julia Chausson interprets seven fairytales, each associated
with a real-estate ad and a key, on the left page, and with a scene
including a house, on the right page.

---

L'exemplaire de la bibliothèque universitaire est numéroté 64/79 et
signé par l'artiste au crayon à papier.



---