---
id: LAJ00030
title: Fables choisies pour les enfants
keywords: Oblong
---

### [[Maurice BOUTET DE MONVEL (1851-1913)]]

#### *Fables choisies pour les enfants*

---

- Paris : [[Plon]], [[Nourrit et Cie]], \[s. d.\].
- Sans ISBN.

- [[Bibliothèque patrimoniale]]

---

Recueil oblong (23 x 27 cm) de vingt-deux fables de La Fontaine dans
lequel Maurice Boutet de Monvel fait de la double page l'espace de
compositions où il exploite les ressources rythmiques des images. La
couverture est en percaline fleurie.

> Oblong (9.1 x 10.6 in) album with twenty-two fables by La Fontaine,
comprising double pages that Maurice Boutet de Monvel fills with
compositions in which he exploits the images' rhythmic resources. With a
floral percaline cover.

---

Il s'agit d'une réédition. L'édition originale, chez Plon, Nourrit et
Cie, date de 1888.



---