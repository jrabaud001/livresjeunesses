---
id: LAJ00052
title: Ali ou Léo ?
keywords: Oblong, conte, tactile, tissu
---

### [[Sophie CURTIL (née en 1949)]]

#### *Ali ou Léo ?*

---

- Talant, Paris : [[Les doigts qui rêvent]], [[Les trois ourses]], 2002.
- ISBN : 2-911782-33-X

- [[Réseau des médiathèques]]

---

Dans ce livre tactile au format oblong (23 x 27 cm), Sophie Curtil
interprète « Le sac prodigieux », un conte des *Mille et une nuits*,
sous la forme de compositions d'empreintes gaufrées réalisées avec
divers objets conservés dans une pochette en tissu cousue à la fin du
livre. Ces compositions sont accompagnées d'un texte en caractères
latins et en braille.

> In this tactile book in oblong (9.1 x 10.6 in) format, Sophie Curtil
interprets "The Stolen Purse", a story from *The Thousand and One
Nights*, through compositions of embossed prints made from different
objects stored in a sewn fabric pouch at the end of the book. These
compositions are accompanied by text in Latin script and in braille.



---