---
id: LAJ00184
title: L'Invitation
keywords: Carré
---

### [[Květa PACOVSKÁ (née en 1928)]]

#### *L'Invitation*

---

- Paris : [[Les grandes personnes]], 2012.
- ISBN : 978-2-36193-152-0

- [[Réseau des médiathèques]]

---

L'artiste invite des motifs récurrents dans son œuvre, du clown au
rhinocéros en passant par les ronds de couleurs, dans les compositions
aux couleurs vives structurées par le rouge et le noir et rehaussées
d'argent de ce livre carré (25 x 25 cm) dont certaines pages sont
pelliculées.

> In this square (9.8 x 9.8 in) book, some pages of which are laminated,
the artist invites recurring motifs in her work, from the clown to the
rhinoceros via coloured circles, into vividly-coloured compositions
structured by red and black with silver highlights.



---