---
id: $identifier$
title: $title$
keywords: $format$$if(subject)$, $for(subject)$$subject$$sep$, $endfor$$endif$
---

### $for(creator)$[[$creator$]]$sep$, $endfor$

#### *$title$*

---

- $~$$for(placeOfPublication)$$placeOfPublication$$sep$, $endfor$ : $for(publisher)$[[$publisher$]]$sep$, $endfor$, $date$$~$.
$if(isbn)$
- ISBN : $for(isbn)$$isbn$$sep$, $endfor$
$else$
- Sans ISBN.
$endif$

- $for(provenance)$[[$provenance$]]$sep$, $endfor$

---

$~$$description-fr$$~$

> $~$$description-en$$~$

$if(abstract)$
---

$~$$abstract$$~$

$endif$

$if(relation)$
---

$relation$

$endif$

---